import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

ReactDOM.render(<App />, document.getElementById("root"));

// const navbar = document.getElementById("navigation-bar");
// const navbarHeight = navbar?.offsetHeight;
// const mainSections = document.getElementsByClassName("main-section");
// const mainHeight = window.innerHeight;

// Array.from(mainSections).forEach((section) => {
//   const elementId = section.id;
//   const currentElement = document.getElementById(elementId);
//   currentElement.style.paddingTop = `${navbarHeight}px`;
// });

// document.getElementById("home").style.height = `${mainHeight - navbarHeight}px`;
