import React, { useState } from "react";
import "../styles/navigation.css";
import homeIcon from "../assets/icons/home-icon.png";
import logo from "../assets/icons/logo.png";
import hamburger from "../assets/icons/hamburger.png";
import { pages } from "../constants.js";

function handleNavigationDisplay(responsive = false) {
  const navigation = document.getElementById("navigation-bar");
  if (navigation.className === "" && responsive) {
    navigation.className += "responsive";
  } else {
    navigation.className = "";
  }
}

function Navigation() {
  const [currentPage, setcurrentPage] = useState(null);

  const getClassName = (pageSelected) => {
    return `navigation-link ${currentPage === pageSelected && `navigation-clicked`
      }`;
  };

  const handleNavigationItemClick = (pageSelected) => {
    setcurrentPage(pageSelected);
    handleNavigationDisplay();
    setTimeout(() => {
      setcurrentPage(null);
    }, 200);
  };

  return (
    <div id="navigation-bar" style={{ zIndex: 2 }}>
      <a
        className={getClassName(pages.HOME)}
        onClick={() => handleNavigationItemClick(pages.HOME)}
        href="#home"
      >
        <img src={homeIcon} alt="home-icon" className="navigation-icon" />
      </a>
      <a
        className={getClassName(pages.SKILLS)}
        onClick={() => handleNavigationItemClick(pages.SKILLS)}
        href="#skills"
      >
        SKILLS
      </a>
      <a
        className={getClassName(pages.PROJECTS)}
        onClick={() => handleNavigationItemClick(pages.PROJECTS)}
        href="#projects"
      >
        PROJECTS
      </a>
      <a
        className={getClassName(pages.ABOUT)}
        onClick={() => handleNavigationItemClick(pages.ABOUT)}
        href="#about"
      >
        ABOUT
      </a>
      <img src={logo} alt="logo" className="navigation-logo" />
      <img
        src={hamburger}
        alt="hamburger"
        className="navigation-hamburger"
        onClick={() => handleNavigationDisplay(true)}
      />
    </div>
  );
}

export default Navigation;
