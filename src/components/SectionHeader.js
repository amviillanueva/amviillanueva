import React from "react";

function SectionHeader(props) {
  return (
    <div id="section-header" data-aos="zoom-out">
      <h1>{props.text}</h1>
    </div>
  );
}

export default SectionHeader;
