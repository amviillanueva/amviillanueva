import React from "react";

function IconWithLink(props) {
  return (
    <div id="icon-with-link" data-aos="slide-left" data-aos-duration="600">
      <img src={props.icon} alt={props.alt} />
      <p>{props.text}</p>
    </div>
  );
}

export default IconWithLink;
