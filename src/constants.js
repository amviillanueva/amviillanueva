// tech stack
import swift from './assets/images/swift.png';
import flutter from './assets/images/flutter.png';
import dart from './assets/images/dart.png';
import git from './assets/images/git.png';
import firebase from './assets/images/firebase.png';
import fastlane from './assets/images/fastlane.png';
// import reactJS from './assets/images/react-js.png';
// import javascript from './assets/images/javascript.png';
// import flutterBloc from './assets/images/flutter-bloc.png';
//import html5 from './assets/images/html5.png';
//import css3 from './assets/images/css3.png';
//import php from './assets/images/php.png';
//import mySql from './assets/images/my-sql.png';

// projects
import standpoint from './assets/images/standpoint.png';
import kargax from './assets/images/kargax.png';
import kargaxTrucker from './assets/images/kargax-trucker.png';
import kargaxDriver from './assets/images/kargax-driver.png';
import luxelips from './assets/images/luxelips.png';
import simonGame from './assets/images/simon-game.png';

// webinars
import zuitt from './assets/images/zuitt.png';
import fcc1 from './assets/images/fcc-responsive.png';
import fcc2 from './assets/images/fcc-js.png';
//import dataScience from './assets/images/data-science-research.png';
//import introToAlgo from './assets/images/intro-to-algo.png';
//import ccna from './assets/images/ccna.png';
//import arduino from './assets/images/arduino.png';
//import HtmlCss from './assets/images/html-css.png';
//import lightroom from './assets/images/lightroom.png';
//import msWord from './assets/images/msword.png';

const pages = {
  HOME: 'home',
  SKILLS: 'skills',
  PROJECTS: 'projects',
  ABOUT: 'about',
};

const techStack = [
  {
    logo: swift,
    name: 'SWIFT'
  },
  {
    logo: flutter,
    name: 'FLUTTER'
  },
  {
    logo: dart,
    name: 'DART'
  },
  {
    logo: git,
    name: 'GIT'
  },
  {
    logo: firebase,
    name: 'FIREBASE'
  },
  {
    logo: fastlane,
    name: 'FASTLANE'
  }
  // {
  //   logo: reactJS,
  //   name: 'REACT JS'
  // },
  // {
  //   logo: javascript,
  //   name: 'JAVASCRIPT'
  // },
  // {
  //   logo: flutterBloc,
  //   name: 'FLUTTER_BLOC'
  // },
//  {
//    logo: html5,
//    name: 'HTML'
//  },
//  {
//    logo: css3,
//    name: 'CSS'
//  },
//  {
//    logo: php,
//    name: 'PHP'
//  },
//  {
//    logo: mySql,
//    name: 'MySQL'
//  }
]

const projects = [
  {
    logo: standpoint,
    name: 'STANDPOINT PH',
    description: 'Standpoint PH is a mobile application which aims to help Filipinos create an educated vote. It allows the verified users to create a post and participate in discussions. In this project, I was tasked to create screen views, add features, connect to API, create a QAT build, and deploy the app on both Android and iOS.',
    androidLink: 'https://play.google.com/store/apps/details?id=ph.standpoint',
    iosLink: 'https://apps.apple.com/ph/app/standpoint-ph/id1598587738',
  },
  {
    logo: kargax,
    name: 'KARGAX',
    description: 'KargaX is a mobile application used for booking deliveries with accredited truckers across the Philippines. It enables the user to monitor the status of the delivery, choose the trucker they prefer, and to negotiate with the trucker. In this project, I was tasked to create screen views, add features, connect to API, create QAT build, and deploy the app on Android.',
    androidLink: 'https://play.google.com/store/apps/details?id=ph.com.kargax.shipper',
  },
  {
    logo: kargaxTrucker,
    name: 'KARGAX TRUCKER',
    description: 'KargaX Trucker is a mobile application that enables truckers to find legitimate shippers, negotiate with clients, assign a driver, and monitor their truck in real time. In this project, I was tasked to create screen views, add features, connect to API, create QAT build, and deploy the app on Android.',
    androidLink: 'https://play.google.com/store/apps/details?id=ph.com.kargax.carrier',
  },
  {
    logo: kargaxDriver,
    name: 'KARGAX DRIVER',
    description: 'KargaX Driver is a mobile application that allows the drivers to check their assigned deliveries, get directions to their destination, and communicate with the recipient. In this project, I was tasked to create screen views, add features, connect to API, create QAT build, and deploy the app on Android.',
    androidLink: 'https://play.google.com/store/apps/details?id=ph.com.kargax.driver',
  },
  {
    logo: luxelips,
    name: 'LUXE LIPS',
    description: 'Luxe lips is a mobile application for a clinic based in Australia. I was one of the front end developers of luxelips and was tasked to create screen views, add functionalities, and make api calls. The app can be downloaded in google playstore.',
    androidLink: 'https://play.google.com/store/apps/details?id=au.com.luxelips',
  },
  {
    logo: simonGame,
    name: 'SIMON GAME',
    description: 'I created this based on the electronic game \'Simon\' using html, css, and vanilla javascript.',
    gameLink: 'http://amviillanueva.gitlab.io/simon-game/'
  },
]

const workExperience = [
  {
    title: 'Maya Philippines — Software Engineer',
    duration: '(May 2022 - Present)',
    description: 'I am currently working in Maya as a Software Engineer, primarily focusing on iOS development. Throughout my tenure, I have developed various solutions that have enhanced the overall user experience.'
  },
  {
    title: 'Exploretale Technologies — Flutter Developer',
    duration: '(September - October 2020 and April 2021 - January 2022)',
    description: 'Worked under Exploretale Technologies as a flutter developer. My work includes connecting to REST APIs, creating and modifying screen views, adding features and functionalities and creating builds for both Android and iOS as well as deploying to appstore and google play. The mobile applications that I worked with are: Standpoint PH, KargaX, KargaX Trucker, KargaX Driver, and Luxe lips.'
  },
  {
    title: 'Ethical Technology — React JS Developer ',
    duration: '(August 2020 - April 2021)',
    description: 'Worked under Ethical Technology as a React JS Developer for an extension in Zoho CRM called Twilio SMS/MMS/Whatsapp Extension for Zoho CRM. My work includes fetching data from Zoho using Zoho CRM SDK and adding new feautures, screen views, and functionalities to the existing system.'
  }
  // ,
  // {
  //   title: 'The Wizard at Worx Holding Inc. — OJT',
  //   duration: '(April 2019 - June 2019)',
  //   description: 'Worked under MIS Department where I did some basic troubleshooting, providing technical support physically and by means of phone call for their employees, and configuring laptops.'
  // }
]

const educationalAchievement = [
  {
    title: 'Computer Programming Skills Competition — 2018',
    description: '3rd Place — held at PUP-ITech',
  },
  {
    title: 'Outstanding Award in Innovation — 2018',
    description: 'Grade 12 Research: Grading System for Senior High School Students in Sumulong College of Arts and Sciences',
  },
  {
    title: 'Festival of Talents — 2015',
    description: 'Participant — held at Antipolo National High School',
  }
]

const webinars = [
  {
    title: 'Zuitt',
    image: zuitt
  },
  {
    title: 'Responsive Web Design',
    image: fcc1
  },
  {
    title: 'Javascript',
    image: fcc2
  },
  // {
  //   title: 'Data Science Research',
  //   image: dataScience
  // },
  // {
  //   title: 'Intro to Algo',
  //   image: introToAlgo
  // },
  // {
  //   title: 'CCNA',
  //   image: ccna
  // },
  // {
  //   title: 'Arduino',
  //   image: arduino
  // },
  // {
  //   title: 'HTML CSS',
  //   image: HtmlCss
  // },
  // {
  //   title: 'Adobe Lightroom',
  //   image: lightroom
  // },
  // {
  //   title: 'MS Word',
  //   image: msWord
  // },
]

export { pages, techStack, projects, workExperience, educationalAchievement, webinars };
