import React from "react";
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';

import SectionHeader from "../components/SectionHeader";
import '../styles/about.css';
import { workExperience } from '../constants';
import laptop from '../assets/icons/laptop.png';
// import medal from '../assets/icons/medal.png';
// import teach from '../assets/icons/teach.png';

function About() {
  return (
    <div id="about-content">
      <Fade top>
        <SectionHeader text='ABOUT' />
      </Fade>
      <div id="about-container">
        <Zoom>
          <div className="about-item">
            <div className="about-header-container">
              <img className="about-header-img" src={laptop} alt="laptop-icon" />
              <p className="about-header">
                WORK EXPERIENCE
              </p>
            </div>
            {
              workExperience.map((experience) => <div className="about-subitems" key={experience.title}>
                <p className="about-title">
                  {experience.title}
                </p>
                <p className="about-subtitle">
                  {experience.duration}
                </p>
                <p className="about-description">
                  {experience.description}
                </p>
              </div>)
            }
          </div>
        </Zoom>
        {/* <Zoom>
          <div className="about-item">
            <div className="about-header-container">
              <img className="about-header-img" src={medal} alt="laptop-icon" />
              <p className="about-header">
                EDUCATIONAL ACHIEVEMENTS
              </p>
            </div>
            {
              educationalAchievement.map((achievement) => <div className="about-subitems" key={achievement.title}>
                <p className="about-title">
                  {achievement.title}
                </p>
                <p className="about-subtitle">
                  {achievement.description}
                </p>
              </div>)
            }
          </div>
        </Zoom> */}
        {/* <Zoom>
          <div className="about-item">
            <div className="about-header-container">
              <img className="about-header-img" src={teach} alt="laptop-icon" />
              <p className="about-header">
                WEBINARS AND ONLINE COURSES
              </p>
            </div>
            <div className="about-webinars">
              {
                webinars.map((webinar) =>
                  <img src={webinar.image} alt={webinar.title} key={webinar.title} />
                )
              }
            </div>
          </div>
        </Zoom> */}


      </div>
    </div>
  );
}

export default About;
