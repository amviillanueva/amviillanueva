import React from "react";
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';

import SectionHeader from "../components/SectionHeader";
import '../styles/projects.css';
import { projects } from '../constants';

function Skills() {
  return (
    <div id="projects-content">
      <Fade>
        <SectionHeader text='PROJECTS' />
      </Fade>
      <div id="projects-container">
        {
          projects.map((project, index) =>
            <Zoom key={project.name + index}>
              <div className="project-item">
                <div>
                  <img src={project.logo} alt={project.name} className="project-img" />
                  <div className="project-details">
                    <p className="project-name">{project.name}</p>
                    <p>{project.description}</p>
                    {
                      project.gameLink ?
                        <a className="project-link" href={project.gameLink} target={project.name} rel="noopener">
                          &gt; Play Game
                        </a>
                        : ''
                    }
                    {
                      project.androidLink ?
                        <a className="project-link" href={project.androidLink} target={project.name} rel="noopener">
                          &gt; Google Play
                        </a>
                        : ''
                    }
                    {
                      project.iosLink ?
                        <a className="project-link" href={project.iosLink} target={project.name} rel="noopener">
                          &gt; App Store
                        </a>
                        : ''
                    }
                  </div>
                </div>
              </div>
            </Zoom>
          )
        }
      </div>
    </div>
  );
}

export default Skills;
