import React from "react";
import "../styles/home.css";

import Tooltip from '@material-ui/core/Tooltip';
import Zoom from 'react-reveal/Zoom';

import profileImage from "../assets/images/my-profile.png";
import IconWithLink from "../components/IconWithLink.js";
import gitlab from "../assets/icons/gitlab.png";
import email from "../assets/icons/email.png";
// import resume from "../assets/resume.pdf";

function Home() {

  // const handleViewResume = () => {
  //   window.open(resume);
  // }

  const profileClick = () => {
    const index = Math.floor(Math.random() * 15);
    const emoji = [
      '(/ω＼)',
      '(*ﾟｰﾟ)ゞ',
      '₍₍ (ง ˙ω˙)ว ⁾⁾',
      '(・・；)',
      '◕_◕',
      '(´～｀ヾ)',
      '༼☯﹏☯༽',
      '(´＿｀)',
      '-`д´-',
      '(-人-)',
      '(｀□′)╯┴┴',
      '(╯－＿－)╯╧╧',
      'ヾ(･ω･｀)',
      '(＊´ω｀人´∀｀＊)',
      '(＾∀＾)'
    ];
    console.log(emoji[index]);
    window.open('https://twitter.com/artbyhosannah');
  }

  return (
    <div id="home-content">
      <div id="home-inner-container">
        <Zoom>
          <div className="profile-image-container">
            <Tooltip title={
              <span>
                art by: sannah <br />
                click me for more info
              </span>
            } arrow>
              <img src={profileImage} alt="profile" className="profile-image" onClick={profileClick} data-aos="zoom-in" />
            </Tooltip>
          </div>
        </Zoom>
        <Zoom cascade>
          <div className="info-container">
            <h1>HI, I'M ANGELIKA.</h1>
            <h3>iOS and Flutter Developer</h3>
            <div><IconWithLink text='gitlab.com/amviillanueva' icon={gitlab} alt='gitlab-logo' /></div>
            <div><IconWithLink text='angelikam.villanueva@gmail.com' icon={email} alt='email-logo' /></div>
            {/* <button className='contact-me-btn' onClick={handleViewResume} data-aos="zoom-in">VIEW RESUME</button> */}
          </div>
        </Zoom>
      </div>
    </div>
  );
}

export default Home;
