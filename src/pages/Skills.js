import React from "react";
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';

import SectionHeader from "../components/SectionHeader";
import '../styles/skills.css';
import { techStack } from '../constants';

function Skills() {
  return (
    <div id="skills-content">
      <Fade top>
        <SectionHeader text='TECH STACK' />
      </Fade>
      <div id="skills-container">
        {
          techStack.map((tech, index) => {
            return <Zoom key={tech.name + index}>
              <div className="skill-item">
                <div>
                  <img src={tech.logo} alt={tech.name} className="skill-img" />
                  <p className="skill-name">{tech.name}</p>
                </div>
              </div>
            </Zoom>
          })
        }
      </div>
    </div>
  );
}

export default Skills;
