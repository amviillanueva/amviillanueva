import "./styles/App.css";
import Navigation from "./components/Navigation";
import Particles from "react-tsparticles";
import particleParams from './assets/particles.json';
import Home from "./pages/Home";
import Skills from "./pages/Skills";
import Projects from "./pages/Projects";
import About from "./pages/About";
// import message from "./assets/icons/message.png";

function App() {
  return (
    <div className="app">
      <Particles params={particleParams} style={{ width: '100%', height: '100%', position: 'fixed', top: 0, zIndex: -1 }} />
      <Navigation />
      <div id="home" className="main-section">
        {/* <Particles params={particleParams} style={{width: '100%', position: 'absolute', top: 0, zIndex: -1}}/> */}
        <Home />
      </div>
      <div id="skills" className="main-section">
        <Skills />
      </div>
      <div id="projects" className="main-section">
        <Projects />
      </div>
      <div id="about" className="main-section">
        <About />
      </div>
      {/* TODO: email functionality */}
      {/* <button className="message-btn">
        <img src={message} alt="mesage-icon" />
      </button> */}
    </div>
  );
}

export default App;
